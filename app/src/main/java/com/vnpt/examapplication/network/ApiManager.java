package com.vnpt.examapplication.network;

import com.vnpt.examapplication.model.request.LoginRequest;
import com.vnpt.examapplication.model.response.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.List;

public interface ApiManager {
    String SERVER = "http://bms.mis.vn/";

    @POST("/api/MobileNhanVien/DangNhap")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);
}
