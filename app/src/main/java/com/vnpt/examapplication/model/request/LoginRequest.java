package com.vnpt.examapplication.model.request;

import android.widget.EditText;

public class LoginRequest {
    private String TenDangNhap;
    private String MatKhau;
    private String MaMay;
    private String MaHeDieuHanh;
    private String TokenFirebase;


    public LoginRequest(String tenDangNhap, String matKhau, String maMay, String maHeDieuHanh, String tokenFirebase) {
        TenDangNhap = tenDangNhap;
        MatKhau = matKhau;
        MaMay = maMay;
        MaHeDieuHanh = maHeDieuHanh;
        TokenFirebase = tokenFirebase;
    }

    public String getTenDangNhap() {
        return TenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        TenDangNhap = tenDangNhap;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String matKhau) {
        MatKhau = matKhau;
    }

    public String getMaMay() {
        return MaMay;
    }

    public void setMaMay(String maMay) {
        MaMay = maMay;
    }

    public String getMaHeDieuHanh() {
        return MaHeDieuHanh;
    }

    public void setMaHeDieuHanh(String maHeDieuHanh) {
        MaHeDieuHanh = maHeDieuHanh;
    }

    public String getTokenFirebase() {
        return TokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        TokenFirebase = tokenFirebase;
    }
}
