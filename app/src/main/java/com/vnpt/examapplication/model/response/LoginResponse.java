package com.vnpt.examapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @Expose
    @SerializedName("DanhSachDonVi")
    private List<DanhSachDonVi> DanhSachDonVi;
    @Expose
    @SerializedName("UserInfo")
    private UserInfo UserInfo;
    @Expose
    @SerializedName("Token")
    private String Token;
    @Expose
    @SerializedName("ErrorDesc")
    private String ErrorDesc;
    @Expose
    @SerializedName("ErrorCode")
    private int ErrorCode;

    public LoginResponse() {
    }

    public List<DanhSachDonVi> getDanhSachDonVi() {
        return DanhSachDonVi;
    }

    public void setDanhSachDonVi(List<DanhSachDonVi> DanhSachDonVi) {
        this.DanhSachDonVi = DanhSachDonVi;
    }

    public UserInfo getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(UserInfo UserInfo) {
        this.UserInfo = UserInfo;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    public String getErrorDesc() {
        return ErrorDesc;
    }

    public void setErrorDesc(String ErrorDesc) {
        this.ErrorDesc = ErrorDesc;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int ErrorCode) {
        this.ErrorCode = ErrorCode;
    }
}
